﻿/*************************************************************/
/*********************  地区下拉框（开始）  *************************/
/*************************************************************/
/**
 * 地区下拉框, *为必输
 * provinceSelector*: 省，默认：.addressProvince
 * citySelector*: 市，默认：.addressCity
 * areaSelector*: 区，默认：.addressArea
 * url*: 数据url，默认："/web/shop/getArea"
 */
 var CODE_SUCCESS = 100;
function AddressWidget(option) {
    this.provinceValue = "";
    this.provinceName = "";
    this.cityValue = "";
    this.cityName = "";
    this.areaValue = "";
    this.areaName = "";

    option = null == option ? {} : option;
    this.provinceSelector = null != option.provinceSelector ? option.provinceSelector : ".addressProvince";
    this.citySelector = null != option.citySelector ? option.citySelector : ".addressCity";
    this.areaSelector = null != option.areaSelector ? option.areaSelector : ".addressArea";
    this.url = null != option.url ? option.url : "/common/getArea";
}

AddressWidget.prototype = {
    init: function() {
        var that = this;
        // 刷新省数据
        that.requestProvince();
        // 绑定事件
        $(that.provinceSelector).on("change", function(){
            that.refresh();
        });
        $(that.citySelector).on("change", function(){
            that.refresh();
        });
        $(that.areaSelector).on("change", function(){
            that.refresh();
        });
    },
    refresh: function(){
        var that = this;
        var newProvinceValue = $(that.provinceSelector).val();
        var newProvinceName = $(that.provinceSelector).children("option:selected").text();
        var newCityValue = $(that.citySelector).val();
        var newCityName = $(that.citySelector).children("option:selected").text();
        var newAreaValue = $(that.areaSelector).val();
        var newAreaName = $(that.areaSelector).children("option:selected").text();
        if(that.provinceValue != newProvinceValue){
            that.provinceValue = newProvinceValue;
			that.provinceName = newProvinceName;
            that.cityValue = "";
            that.cityName = "";
            that.areaValue = "";
            that.areaName = "";
            that.requestCity();
            that.requestArea();
        }else if(that.cityValue != newCityValue){
            that.cityValue = newCityValue;
            that.cityName = newCityName;
            that.areaValue = "";
            that.areaName = "";
            that.requestArea();
        }else{
            that.areaValue = newAreaValue;
            that.areaName = newAreaName;
        }
    },
    requestProvince: function(){
        var that = this;
        that.createProvince([]);
        $.post(that.url, {}, function(result){
            if(CODE_SUCCESS != result.code){
                alert(result.message);
                return;
            }
            that.createProvince(result.object);
        }, "json");
    },
    requestCity: function(){
        var that = this;
        that.createCity([]);
        if("" == that.provinceValue){
            return;
        }
        $.post(that.url, {id: that.provinceValue, level: 2}, function(result){
            if(CODE_SUCCESS != result.code){
                alert(result.message);
                return;
            }
            that.createCity(result.object);
        }, "json");
    },
    requestArea: function(){
        var that = this;
        that.createArea([]);
        if("" == that.cityValue){
            return;
        }
        $.post(that.url, {id: that.cityValue, level: 3}, function(result){
            if(CODE_SUCCESS != result.code){
                alert(result.message);
                return;
            }
            that.createArea(result.object);
        }, "json");
    },
    createProvince: function(provinceList){
        var that = this;
        var html = '<option value="">请选择省份</option>';
        for(var i = 0; i < provinceList.length; i++){
            var obj = provinceList[i];
            html += '<option value="' + obj.id + '">' + obj.name + '</option>';
        }
        $(that.provinceSelector).html(html);
    },
    createCity: function(cityList){
        var that = this;
        var html = '<option value="">请选择城市</option>';
        for(var i = 0; i < cityList.length; i++){
            var obj = cityList[i];
            html += '<option value="' + obj.id + '">' + obj.name + '</option>';
        }
        $(that.citySelector).html(html);
    },
    createArea: function(areaList){
        var that = this;
        var html = '<option value="">请选择区县</option>';
        for(var i = 0; i < areaList.length; i++){
            var obj = areaList[i];
            html += '<option value="' + obj.id + '">' + obj.name + '</option>';
        }
        $(that.areaSelector).html(html);
    }
};
/*************************************************************/
/*********************  地区下拉框（结束）  *************************/
/*************************************************************/
