<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/_common.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>恭贺新禧</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="description" content="fullPage:For desktop(ie5.5+) or mobile webApp without jQuery,create full screen pages fast and simple.">
	<meta name="keywords" content="fullPage,webApp,full screen">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
	<link rel="stylesheet" type="text/css" href="${res}/css/main.css?v=1">
	<link rel="stylesheet" type="text/css" href="${res}/css/animate.css">
</head>
<!--[if lte IE 7]>
<body scroll="no">
<![endif]-->
<!--[if gt IE 7]><!-->

<body>
	<!--<![endif]-->

	<div id="article">
		<div class="page section1 current">
			<div class="contain">
				<img class="animated bounceInDown" src="${res}/images/jimu/ji.png" style="margin-top:30px;" />
				<img class="animated zoomIn anmationDelay5" src="${res}/images/jimu/jimuword.png" style="margin-top:15px;height:8rem" />
				<img class="animated zoomIn anmationDelay10" src="${res}/images/jimu/logo.png" style="position:absolute;bottom:3rem;width:60%;left:20%" />
			</div>
		</div>

		<%-- <div class="page section2">
			<div class="contain">
				<img class="animated bounceInDown" src="${res}/images/jimu/ji.png" style="margin-top:30px;" />
				<img class="animated zoomIn anmationDelay5" src="${res}/images/jimu/jimuword.png" style="margin-top:15px;height:8rem" />
				<img class="animated zoomIn anmationDelay10" src="${res}/images/jimu/logo.png" style="position:absolute;bottom:3rem;width:60%;left:20%" />
			</div>
		</div> --%>
	</div>
	<%-- <img src="${res}/images/jimu/arrow.png" style="" id="array" class="resize"> --%>
	<div id="music" class="music">
		<!-- stopped -->
		<audio id="music-audio" onplay="this.currentTime=0" class="audio" loop="" autoplay="autoplay" preload="">
			<source src="${res}/mp3/newYear.mp3">
		</audio>
		<div class="control">
			<div class="control-after"></div>
		</div>
	</div>
  <script src="http://apps.bdimg.com/libs/zepto/1.1.4/zepto.min.js"></script>
	<script type="text/javascript" src="${res}/js/fullPage.min.js"></script>
	<script type="text/javascript">
		runSection = new FullPage({

			id: 'article', // id of contain
			slideTime: 800, // time of slide
			effect: { // slide effect
				transform: {
					translate: 'Y', // 'X'|'Y'|'XY'|'none'
					scale: [0, 1], // [scalefrom, scaleto]
					rotate: [0, 45] // [rotatefrom, rotateto]
				},
				opacity: [0, 1] // [opacityfrom, opacityto]
			},
			mode: 'touch', // mode of fullpage
			easing: [0, .93, .39, .98],
			callback: function(index, thisPage) { // callback when pageChange

			}
		});
    var myAudio = document.getElementById('music-audio');
    $('#music').click(function () {
      if(myAudio.paused) {
        myAudio.play();
        $(this).removeClass('stopped');
      }else {
        myAudio.pause();
        $(this).addClass('stopped');
      }
    });
		// 初始化播放Audio;
		document.getElementById('music-audio').play();
		$(document).one('touchstart',function () {
			document.getElementById('music-audio').play();
		});
	</script>

	<!--[if lte IE 6]>
	<script type="text/javascript" src="http://mat1.gtimg.com/cd/201108/app/DD_belatedPNG_0.0.8a.js" charset="utf-8"></script>
	<script type="text/javascript">
	DD_belatedPNG.fix("*");
	</script>
	<![endif]-->
	<script type="text/javascript" src='http://res.wx.qq.com/open/js/jweixin-1.0.0.js'></script>
	<script>
	  wx.config({
	      debug: false,
	      appId: "${config.appId}",
	      timestamp: "${config.timestamp}",
	      nonceStr: "${config.nonceStr}",
	      signature: "${config.signature}",
	      jsApiList: [
	        'onMenuShareTimeline','onMenuShareAppMessage'
	      ]
	  });
	  wx.ready(function () {
		  wx.onMenuShareTimeline({
		      title: '恭贺新禧', // 分享标题
		      link: "${proUrl}/weixin/share", // 分享链接
		      imgUrl: "${proUrl}/assets/blue/images/jimu/ji.png", // 分享图标
		      success: function () {
		      },
		      cancel: function () {
		          // 用户取消分享后执行的回调函数
		      }
		    });
		    wx.onMenuShareAppMessage({
		      title: '恭贺新禧', // 分享标题
		      desc: '宁波市侨办周敏华', // 分享描述
		      link: "${proUrl}/weixin/share", // 分享链接
		      imgUrl: "${proUrl}/assets/blue/images/jimu/ji.png", // 分享图标
		      type: '', // 分享类型,music、video或link，不填默认为link
		      dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
		      success: function () {
		          // 用户确认分享后执行的回调函数
		      },
		      cancel: function () {
		          // 用户取消分享后执行的回调函数
		      }
	    });
	  });
	</script>

</body>

</html>
