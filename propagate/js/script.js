/*********旋转屏幕************/
var width = document.documentElement.clientWidth;
var height = document.documentElement.clientHeight;
if (width < height) {
    console.log(width + " " + height);
    $print = $('#print');
    $print.width(height);
    $print.height(width);
    $print.css('top', (height - width) / 2);
    $print.css('left', 0 - (height - width) / 2);
    $print.css('transform', 'rotate(90deg)');
    $print.css('transform-origin', '50% 50%');
}
// 用户开启旋转屏幕
var evt = "onorientationchange" in window ? "orientationchange" : "resize";
window.addEventListener(evt, function() {
    console.log(evt);
    var width = document.documentElement.clientWidth;
    var height = document.documentElement.clientHeight;
    $print = $('#print');
    console.log('width:' + width);
    console.log('height:' + height);
    if (width > height) {
        $print.width(width);
        $print.height(height);
        $print.css('top', 0);
        $print.css('left', 0);
        $print.css('transform', 'none');
        $print.css('-webkit-ransform', 'none');
        $print.css('-moz-transform', 'none');
        $print.css('-o-transform', 'none');
        $print.css('transform-origin', '50% 50%');
        $print.css('-webkit-transform-origin', '50% 50%');
        $print.css('-moz-transform-origin', '50% 50%');
        $print.css('-o-transform-origin', '50% 50%');
    } else {
        $print.width(height);
        $print.height(width);
        $print.css('top', (height - width) / 2);
        $print.css('left', 0 - (height - width) / 2);
        $print.css('transform', 'rotate(90deg)');
        $print.css('-webkit-ransform', 'rotate(90deg)');
        $print.css('-moz-transform', 'rotate(90deg)');
        $print.css('-o-transform', 'rotate(90deg)');
        $print.css('transform-origin', '50% 50%');
        $print.css('-webkit-transform-origin', '50% 50%');
        $print.css('-moz-transform-origin', '50% 50%');
        $print.css('-o-transform-origin', '50% 50%');
    }
}, false);
