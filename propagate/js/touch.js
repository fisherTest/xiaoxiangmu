var wrap=document.getElementById("wrap");
var inner=document.getElementById("inner");
var left=document.getElementById("left");
var right=document.getElementById("right");
var music2 = document.getElementById("music2");
var Jaudio = document.getElementById("Jaudio");
var clickFlag=true;//设置左右切换标记位防止连续按
var time//主要用来设置自动滑动的计时器
var index=0;//记录每次滑动图片的下标
var distance=$('#wrap').width();//获取展示区的宽度，即每张图片的宽度

$(left).on('touchstart',function () {
  music2.play();
  // Jaudio.pause();
  // setTimeout(function () {
  //   Jaudio.play();
  // },1000);
  goLeft();
});
$(right).on('touchstart',function () {
  music2.play();
  // Jaudio.pause();
  // setTimeout(function () {
  //   Jaudio.play();
  // },1000);
  goRight();
});

function goLeft() {
  index--;
  $(right).show();
  $(inner).css({
    'transform':'translateX(-'+distance*index+'px)'
  });
  if(index == 0) {
    $(left).hide();
    return false;
  }
}
function goRight() {
  index++;
  $(left).show();
  $(inner).css({
    'transform':'translateX(-'+distance*index+'px)'
  });
  console.log(index);
  if(index == 2 ) {
    $(right).hide();
    return false
  }
}
function initCase() {
  $(left).hide();
  $(right).show();
  index=0;
  $(inner).css({
    'transform':'translateX(0px)'
  });
}

// 触摸滑动
// var touchstartX = 0;
// var touchendX = 0;
// touchFn({selector:wrap,touchstartX:touchstartX,touchendX:touchendX});
// function touchFn(options) {
//     var selector = options.selector;
//     var touchstartX = options.touchstartX;
//     var touchendX = options.touchendX;
//     load(selector,touchstartX,touchendX);
// }
//
// function load(selector,touchstartX,touchendX) {
//     selector.addEventListener('touchstart', touch, false);
//     // selector.addEventListener('touchmove', touch, false);
//     selector.addEventListener('touchend', touch, false);
//     function touch(event) {
//         var event = event || window.event;
//         switch (event.type) {
//             case "touchstart":
//                 // console.log("Touch started (" + event.touches[0].clientX + "," + event.touches[0].clientY + ")");
//                 touchstartX = event.touches[0].clientX;
//                 break;
//             case "touchend":
//                 // console.log("Touch end(" + event.changedTouches[0].clientX + ", " + event.changedTouches[0].clientY + ")");
//                 touchendX = event.changedTouches[0].clientX;
//                 break;
//             case "touchmove":
//                 event.preventDefault();
//                 // console.log("Touch moved(" + event.touches[0].clientX + ", " + event.touches[0].clientY + ")");
//                 break;
//         }
//         calculate(touchstartX,touchendX);
//     }
// }
//
// function calculate(touchstartX,touchendX) {
//   // console.log(touchstartX,touchendX);
//   if(touchendX < touchstartX) {
//     if(index == 2) {
//       return false;
//     }
//     goRight();
//   }else {
//     if(index == 0) {
//       return false;
//     }
//     goLeft();
//   }
// }
