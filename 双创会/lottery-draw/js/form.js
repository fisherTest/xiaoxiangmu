function Validate(options) {

}
Validate.prototype = {
  /***
    *@param arr {Array} 需要传入验证的内容id数组
    *@param labelArr {Array} 需要传入验证的提醒内容数组
    *@return Boolean
    **/
    checkNotNone:function (arr,labelArr) {
        var item;
        for (var i = 0; i < arr.length; i++) {
          item = $('#'+arr[i]).val();
          // item = document.getElementById(arr[i]).value;
          if(item == "" || item == 0) {
            alert(labelArr[i]);
            return false;
          }
        }
        return true;
    },
    checkPhone:function (mobile) {
        var reg = /^(13[0-9]|14[0-9]|15[0-9]|18[0-9])\d{8}$/i;
        if (!reg.test(mobile)) {
            alert("请输入正确的手机号！");
            return false;
        }
        return true;
    },
    getQueryString:function () {
        var obj = {};
        var url = window.location.search;
        if (url.indexOf("?") != -1) {
            var str = url.substr(1);
            var strs = str.split("&");
            for (var i = 0; i < strs.length; i++) {
                obj[strs[i].split("=")[0]] = (strs[i].split("=")[1]);
            }
            return obj;
        }
    }
};
$('#submit').click(function () {

  var name = $('#name').val(),
      companyName = $('#companyName').val(),
      mobile = $('#mobile').val();
  var arr = ['name', 'mobile'];
  var labelArr = ['姓名不能为空', '手机号不能为空'];
  var validate = new Validate();//验证表单

  var params = {
    name:name,
    companyName:companyName,
    mobile:mobile,
    key:validate.getQueryString().key
  }

  if(validate.checkNotNone(arr,labelArr)) {
    if(validate.checkPhone(mobile)) {
      // 这里进行ajax进行表单提交
      $.post(HOST,params,function (data) {
        if(data.code != 100) {
          alert(data.message);
          return false;
        }
        window.location.href = "success.html"
      },'json')
    }
  }
})
